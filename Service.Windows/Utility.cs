﻿#region Related components
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Threading;
using System.Threading.Tasks;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
#endregion

namespace net.vieapps.Services.Importers
{
	public static partial class Utility
	{

		#region Working with data-table & data-reader
		internal static DataTable CreateDataTable(this DbDataReader dataReader, string name = "Table", bool doLoad = false)
		{
			var dataTable = new DataTable(name);

			if (doLoad)
				dataTable.Load(dataReader);

			else
				foreach (DataRow info in dataReader.GetSchemaTable().Rows)
				{
					var dataColumn = new DataColumn();
					dataColumn.ColumnName = info["ColumnName"].ToString();
					dataColumn.Unique = Convert.ToBoolean(info["IsUnique"]);
					dataColumn.AllowDBNull = Convert.ToBoolean(info["AllowDBNull"]);
					dataColumn.ReadOnly = Convert.ToBoolean(info["IsReadOnly"]);
					dataColumn.DataType = (Type)info["DataType"];
					dataTable.Columns.Add(dataColumn);
				}

			return dataTable;
		}

		internal static void Append(this DataTable dataTable, DbDataReader dataReader)
		{
			var data = new object[dataReader.FieldCount];
			for (var index = 0; index < dataReader.FieldCount; index++)
				data[index] = dataReader[index];
			dataTable.LoadDataRow(data, true);
		}

		internal static DataTable ToDataTable(this DbDataReader dataReader, string name = "Table")
		{
			var dataTable = dataReader.CreateDataTable(name);
			while (dataReader.Read())
				dataTable.Append(dataReader);
			return dataTable;
		}

		internal static async Task<DataTable> ToDataTableAsync(this DbDataReader dataReader, string name = "Table", CancellationToken cancellationToken = default(CancellationToken))
		{
			var dataTable = dataReader.CreateDataTable(name);
			while (await dataReader.ReadAsync(cancellationToken).ConfigureAwait(false))
				dataTable.Append(dataReader);
			return dataTable;
		}
		#endregion

	}
}