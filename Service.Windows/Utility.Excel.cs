﻿#region Related components
using System;
using System.IO;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Threading;
using System.Threading.Tasks;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
#endregion

namespace net.vieapps.Services.Importers
{
	internal static partial class ExcelUtility
	{
		public static DataTable ReadExcelDocument(string filePath, string worksheet = "Sheet1")
		{
			// check
			if (string.IsNullOrWhiteSpace(filePath))
				throw new ArgumentNullException(nameof(filePath), "File path is invalid");
			else if (!File.Exists(filePath))
				throw new FileNotFoundException($"The Excel file is not found ({filePath})");

			// default name of Excel-Reader provider is for Windows 2008 and higher
			var providerName = "Microsoft.ACE.OLEDB.12.0";

			// Excel on Windows 2003 or lower
			try
			{
				if (Environment.OSVersion.VersionString.Contains("Microsoft Windows NT 5"))
					providerName = "Microsoft.Jet.OLEDB.4.0";
			}
			catch { }

			// fetch data
			using (var connection = new OleDbConnection($"Provider={providerName};Data Source={filePath};Extended Properties=\"Excel 8.0;HDR=Yes;\""))
			{
				connection.Open();
				var command = new OleDbCommand($"SELECT * FROM [{worksheet}$]", connection);
				using (var dataReader = command.ExecuteReader())
				{
					return dataReader.ToDataTable(worksheet);
				}
			}
		}

		public static async Task<DataTable> ReadExcelDocumentAsync(string filePath, string worksheet = "Sheet1", CancellationToken cancellationToken = default(CancellationToken))
		{
			// check
			if (string.IsNullOrWhiteSpace(filePath))
				throw new ArgumentNullException(nameof(filePath), "File path is invalid");
			else if (!File.Exists(filePath))
				throw new FileNotFoundException($"The Excel file is not found ({filePath})");

			// default name of Excel-Reader provider is for Windows 2008 and higher
			var providerName = "Microsoft.ACE.OLEDB.12.0";

			// Excel on Windows 2003 or lower
			try
			{
				if (Environment.OSVersion.VersionString.Contains("Microsoft Windows NT 5"))
					providerName = "Microsoft.Jet.OLEDB.4.0";
			}
			catch { }

			// fetch data
			using (var connection = new OleDbConnection($"Provider={providerName};Data Source={filePath};Extended Properties=\"Excel 8.0;HDR=Yes;\""))
			{
				await connection.OpenAsync(cancellationToken).ConfigureAwait(false);
				var command = new OleDbCommand("SELECT * FROM [" + worksheet + "$]", connection);
				using (var dataReader = await command.ExecuteReaderAsync(cancellationToken).ConfigureAwait(false))
				{
					return await dataReader.ToDataTableAsync(worksheet, cancellationToken).ConfigureAwait(false);
				}
			}
		}
	}
}