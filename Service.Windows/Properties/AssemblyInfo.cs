﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: ComVisible(false)]

[assembly: AssemblyTitle("VIEApps NGX Importers")]
[assembly: AssemblyCompany("VIEApps.net")]
[assembly: AssemblyProduct("VIEApps NGX")]
[assembly: AssemblyCopyright("© 2019 VIEApps.net")]

[assembly: AssemblyVersion("10.2.1904.3")]
[assembly: AssemblyFileVersion("10.2.1904.3")]
[assembly: AssemblyInformationalVersion("v10.1.netframework-4.7.2+rev:2010.04.11-latest.components")]